"""
CountdownTimer - Controller
"""

from kivy.app import App
from kivy.clock import Clock
from kivy.uix.gridlayout import GridLayout


from view import TitleView, CountdownTimerView, ImageView
from model import CountdownTimer

__author__ = "jtran"
__version__ = "0.1"


class CountdownTimerController(App):
    def __init__(self, **kwargs):
        super(CountdownTimerController, self).__init__()
        self.title = "Countdown Timer"

        title = kwargs.get("title", "")
        image = kwargs.get("image", None)

        year = kwargs.get("year", 2020)
        month = kwargs.get("month", 1)
        day = kwargs.get("day", 1)

        self._main_grid = GridLayout()
        self._main_grid.cols = 1

        self._title_view = TitleView(title=title)
        self._countdowntimer_view = CountdownTimerView()
        self._image_view = None if image is None else ImageView(image=image)

        self._countdowntimer = CountdownTimer(year, month, day)

        Clock.schedule_interval(self._update_counter, 0.001)

    """
    Private methods
    """
    def _update_counter(self, args):
        remaining_time = self._countdowntimer.remaining_time
        self._countdowntimer_view.update_years(remaining_time.years)
        self._countdowntimer_view.update_months(remaining_time.months)
        self._countdowntimer_view.update_days(remaining_time.days)
        self._countdowntimer_view.update_hours(remaining_time.hours)
        self._countdowntimer_view.update_minutes(remaining_time.minutes)
        self._countdowntimer_view.update_seconds(remaining_time.seconds)

    """
    Overloaded methods
    """
    def build(self):
        self._main_grid.add_widget(self._title_view)
        self._main_grid.add_widget(self._countdowntimer_view)
        if self._image_view is not None:
            self._main_grid.add_widget(self._image_view)
        return self._main_grid




if __name__ == "__main__":  # Test
    controller = CountdownTimerController(title="Test Controller", image="pp.jpg", year=2020)
    controller.run()
