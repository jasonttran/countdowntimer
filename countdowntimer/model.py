"""
CountdownTimer - Model
"""

from collections import namedtuple
from datetime import datetime

from dateutil.relativedelta import relativedelta

__author__ = "jtran"
__version__ = "0.1"

DeltaTime = namedtuple("DeltaTime", ["years", "months", "days", "hours", "minutes", "seconds"])


class CountdownTimer(object):
    def __init__(self, year, month=1, day=1):
        self._datetime = datetime(year=year, month=month, day=day)
        assert datetime.now() < self._datetime, "The specified date [{}] must be in the future!".format(self._datetime)

    """
    Accessors
    """
    @property
    def remaining_time(self):
        now = datetime.now()
        delta = relativedelta(dt1=self._datetime, dt2=datetime(year=now.year, month=now.month, day=now.day, hour=now.hour, minute=now.minute, second=now.second))
        if len(filter(lambda count: count < 0, [delta.years, delta.months, delta.days, delta.hours, delta.minutes, delta.seconds])) > 0:
            deltatime = DeltaTime(0, 0, 0, 0, 0)
        else:
            deltatime = DeltaTime(years=delta.years, months=delta.months, days=delta.days, hours=delta.hours, minutes=delta.minutes, seconds=delta.seconds)
        return deltatime




if __name__ == "__main__":  # Test
    try:
        CountdownTimer(2011, 5, 2)
        AssertionError("Expected AssertionError")
    except AssertionError:
        pass

    _cdt = CountdownTimer(2100)
    _delta = _cdt.remaining_time
    print _delta.years, _delta.months, _delta.days, _delta.hours, _delta.minutes, _delta.seconds
