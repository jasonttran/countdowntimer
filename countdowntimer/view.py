"""
CountdownTimer - View
"""

from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.image import Image

__author__ = "jtran"
__version__ = "0.1"


class TitleView(GridLayout):
    def __init__(self, **kwargs):
        super(TitleView, self).__init__(**kwargs)
        self.cols = 1

        title = kwargs.get("title", "")
        self._title_label = Label(text=title, halign="center", font_size="60sp")
        self.add_widget(self._title_label)


class CountdownTimerView(GridLayout):
    def __init__(self, **kwargs):
        super(CountdownTimerView, self).__init__(**kwargs)

        self.cols = 6  # Years, Months, Days, Hours, Minutes, Seconds

        """ Widgets """
        self._years_entry = Label(text="", halign="center", font_size="30sp")
        self._months_entry = Label(text="", halign="center", font_size="30sp")
        self._days_entry = Label(text="", halign="center", font_size="30sp")
        self._hours_entry = Label(text="", halign="center", font_size="30sp")
        self._minutes_entry = Label(text="", halign="center", font_size="30sp")
        self._seconds_entry = Label(text="", halign="center", font_size="30sp")

        """ Add widgets """
        self.add_widget(self._years_entry)
        self.add_widget(self._months_entry)
        self.add_widget(self._days_entry)
        self.add_widget(self._hours_entry)
        self.add_widget(self._minutes_entry)
        self.add_widget(self._seconds_entry)

        """ Initialize widgets"""
        self.update_years("0")
        self.update_months("0")
        self.update_days("0")
        self.update_hours("0")
        self.update_minutes("0")
        self.update_seconds("0")

    """
    Public methods
    """
    def update_years(self, value):
        self._years_entry.text = "{}\n\nYears".format(str(value))

    def update_months(self, value):
        self._months_entry.text = "{}\n\nMonths".format(str(value))

    def update_days(self, value):
        self._days_entry.text = "{}\n\nDays".format(str(value))

    def update_hours(self, value):
        self._hours_entry.text = "{}\n\nHours".format(str(value))

    def update_minutes(self, value):
        self._minutes_entry.text = "{}\n\nMinutes".format(str(value))

    def update_seconds(self, value):
        self._seconds_entry.text = "{}\n\nSeconds".format(str(value))


class ImageView(GridLayout):
    def __init__(self, **kwargs):
        super(ImageView, self).__init__(**kwargs)
        self.cols = 1

        image = kwargs.get("image", None)
        if image is not None:
            self._image = Image(source=image, keep_ratio=True)
            self.add_widget(self._image)


if __name__ == "__main__":  # Test
    from kivy.app import App

    class TestApp(App):
        def build(self):
            self.title = "Self Test"

            grid = GridLayout()
            grid.cols = 1
            grid.add_widget(TitleView(title="Test View"))
            grid.add_widget(CountdownTimerView())
            grid.add_widget(ImageView(image="pp.jpg"))
            return grid


    TestApp().run()
