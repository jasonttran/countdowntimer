from collections import namedtuple
import json
import sys

from countdowntimer import CountdownTimer

__author__ = "jtran"
__version__ = "0.1"

COUNTDOWNTIMER_CONFIG = "countdowntimer_settings.json"

Config = namedtuple("Config", ["title", "image", "year", "month", "day"])


def main():
    config = read_config()
    countdowntimer = CountdownTimer(title=config.title,
                                    image=config.image,
                                    year=config.year,
                                    month=config.month,
                                    day=config.day)
    countdowntimer.run()
    return 0


def read_config():
    with open(COUNTDOWNTIMER_CONFIG) as file_obj:
        json_dict = json.load(file_obj)

    config = Config(title=json_dict["TITLE"],
                    image=json_dict.get("IMAGE", None),
                    year=json_dict["DATE"]["YEAR"],
                    month=json_dict["DATE"]["MONTH"],
                    day=json_dict["DATE"]["DAY"])
    return config


if __name__ == "__main__":
    sys.exit(main())
